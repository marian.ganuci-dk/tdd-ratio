# TDD Ratio

This is the exercise proposed during the technical workshop on TDD - Jakarta (September 2022)

1. Create a small utility which is able to scan a software project (in the form of a local folder) and output the ratio between lines of tests and lines of code.

Notes: 
* It should support NodeJS projects.

Assumptions:
* program input would be the project folder that needs to be used for searching sources and tests 
* for NodeJS projects, one of the criteria for identifying tests are by using folder names __tests__ would only contain tests


2. Enhance the existing utility version to output the ratio between number of code methods and number of tests.

Notes:
* The existing program should continue to function without errors while developing the new version


3. Expand the previous version of the utility to consider NodeJS and Kotlin projects.

Notes: 
* The existing program should continue to function without errors while developing the new version
