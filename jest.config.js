module.exports = {
  modulePathIgnorePatterns: ["resources"],
  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.js',
    "*.js",
    "!jest.config.js",
    '!**/node_modules/**',
    '!**/resources/**',
    '!**/coverage/**'
  ]
};
