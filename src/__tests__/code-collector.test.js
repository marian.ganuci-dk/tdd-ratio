const codeCollector = require('../code-collector')

describe("Code collector tests", () => {
    test("No source test", () => {
        expect(codeCollector.getTestAndCodeLines("./resources/project1/src").codeLines).toBe(0)
    });

    test("Single source file test", () => {
        expect(codeCollector.getTestAndCodeLines("./resources/project2/src").codeLines).toBe(2)
    });
    
    test("Multiple source files same level", () => {
        expect(codeCollector.getTestAndCodeLines("./resources/project3/src").codeLines).toBe(10)
    });

    test("Source ignores white spaces", () => {
        expect(codeCollector.getTestAndCodeLines("./resources/project4").codeLines).toBe(4)
    });    

    test("Source collected from multiple levels", () => {
        expect(codeCollector.getTestAndCodeLines("./resources/project5/src").codeLines).toBe(20)
    });

    test("Source collected l+2", () => {
        expect(codeCollector.getTestAndCodeLines("./resources/project6/src").codeLines).toBe(30)
    });          

    test("Mocks are skipped", () => {
        expect(codeCollector.getTestAndCodeLines("./resources/project7/src").codeLines).toBe(10)
    });          
})