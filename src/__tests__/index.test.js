const { spawnSync} = require('child_process');

describe("API / process tests", () => {

    test("Invocation uses no arguments", () => {
        const child = spawnSync('node', ['src/index.js'])
        expect(child.status).toBe(1)
        expect(child.stderr.toString('utf-8')).toMatch("Folder must be provided")
    });

    test("Invocation using a file instead of folder", () => {
        const child = spawnSync('node', ['src/index.js', './src/index.js'])
        expect(child.status).toBe(1)
        expect(child.stderr.toString('utf-8')).toMatch("Path is not a folder")
    });
    
    test("Invocation using a non-existent folder", () => {
        const child = spawnSync('node', ['src/index.js', './resources/project_0'])
        expect(child.status).toBe(1)
        expect(child.stderr.toString('utf-8')).toMatch("Path does not exist")
    });    

    test("Invocation against a project with no sources", () => {
        const child = spawnSync('node', ['src/index.js', './resources/project1/src'])
        expect(child.status).toBe(1)
        expect(child.stderr.toString('utf-8')).toMatch("No source identified")
    });
    
    test("Expect result displays absolute paths in output string", () => {
        const child = spawnSync('node', ['src/index.js', './resources/project5'])
        expect(child.status).toBe(0)
        expect(child.stdout.toString('utf-8')).not.toMatch("./resources/project5")
    });    

    test("Expect result displays source lines and test lines count in output string", () => {
        const child = spawnSync('node', ['src/index.js', './resources/project4'])
        expect(child.status).toBe(0)
        expect(child.stdout.toString('utf-8')).toMatch("4/4")
    });    

});