const collector = require('../code-collector')

describe("Test Collector Tests", () => {

    test("Single file test (1)", () => {
        expect(collector.getTestAndCodeLines("./resources/project1/src").testLines).toBe(3)
    })

    test("Identified in __tests__ folder", () => {
        expect(collector.getTestAndCodeLines("./resources/project2/src").testLines).toBe(4)
    })    

    test("Summed up in __tests__ folder", () => {
        expect(collector.getTestAndCodeLines("./resources/project3/src").testLines).toBe(13)
    })   

    test("Empty lines are ignored in tests", () => {
        expect(collector.getTestAndCodeLines("./resources/project4").testLines).toBe(4)
    })    

    test("Summed up recursively from __tests__ no matter the name", () => {
        expect(collector.getTestAndCodeLines("./resources/project5/src").testLines).toBe(8)
    })    

    test("Mocks are ignored if at same level as __tests__", () => {
        expect(collector.getTestAndCodeLines("./resources/project7/src").testLines).toBe(11)
    })  

    test("Tests are collected from all source subfolders", () => {
        expect(collector.getTestAndCodeLines("./resources/project8/src").testLines).toBe(40)
    })      

})