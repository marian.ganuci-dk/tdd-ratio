const validator = require('../validator');

describe("Validator tests", () => {
    test("No argument provided", () => {
        expect(validator.validate()).toBe("No path provided")
    });
    
    test("Path is not a folder", () => {
        expect(validator.validate("./src/index.js")).toBe("Path is not a folder")
    });    

    test("Folder does not exists", () => {
        expect(validator.validate("./resources/project-not-existing")).toBe("Path does not exist")
    });     

    test("Path is a valid folder", () => {
        expect(validator.validate("./resources/project1")).toBeUndefined()
    });        
})