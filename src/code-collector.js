const fs = require('fs');
const path = require('path')

getTestAndCodeLines = (projectFolder) => {
    var folders = []
    collectFolders(projectFolder, folders)

    return {
        codeLines : getSumByFilter(folders, f => f.indexOf("__tests__") == -1 && f.indexOf("__mocks__") == -1),
        testLines : getSumByFilter(folders, f => f.indexOf("__tests__") != -1)
    }
}

const getSumByFilter = (folders, filterFn) => {
    return folders
        .filter(filterFn)
        .map(f => fileCountFromFolder(f))
        .reduce((a, b) => a + b, 0)
}

fileCountFromFolder = folder => {
    return fs.readdirSync(folder, { encoding: 'utf-8', withFileTypes: true })
        .filter(direEnt => direEnt.isFile())
        .map(f => fileLineCount(path.join(folder, f.name)))
        .reduce((a, b) => a + b, 0)
}

fileLineCount = (file) => {
    const content = fs.readFileSync(file, 'utf-8')
    return content.split(/\n/).filter(line => line.trim().length > 0).length
}

collectFolders = (folder, arr) => {
    arr.push(folder)
    fs.readdirSync(folder, { encoding: 'utf-8', withFileTypes: true })
        .filter(dirEnt => dirEnt.isDirectory())
        .forEach(dirEnt => collectFolders(path.join(folder, dirEnt.name), arr))
}

module.exports.getTestAndCodeLines = getTestAndCodeLines