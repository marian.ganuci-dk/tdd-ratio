const codeCollector = require('./code-collector')
const validator = require('./validator');
const path = require('path');

if(process.argv.length != 3) {
    console.error("Folder must be provided")
    process.exit(1)
}

let error = validator.validate(process.argv[2])
if(error) {
    console.error(error)
    process.exit(1)
}

const result = codeCollector.getTestAndCodeLines(process.argv[2])

if(result.codeLines == 0) {
    console.error("No source identified")
    process.exit(1)
}

console.log(`${result.testLines/result.codeLines} (${result.testLines}/${result.codeLines}) for ${path.resolve(process.argv[2])}`)