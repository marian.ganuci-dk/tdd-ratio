const fs = require('fs')

//  Performs a validation against the incoming path. If there's an error detected it will be returned
// Checks for the agument to be an existing folder
const validate = (path) => {
    if(undefined == path) {
        return "No path provided"
    }

    if(!fs.existsSync(path)) {
        return "Path does not exist"
    }

    if(!fs.statSync(path).isDirectory()) {
        return "Path is not a folder"
    }
}

module.exports.validate = validate